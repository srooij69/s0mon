# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Waar is deze repository voor? ###

* Deze repository is bedoeld voor mijn uitbreiding op p1mon van www.ztatz.nl 
* Huidige versie 0.01

* Mijn uitbreiding bestaat uit:
-  een python 'service' die s0 pulsen op een GPIO poort logt in een SQLite db 
* Wensen:
-  php web pagina's om de gelogde waarden te tonen


### Werking ###
s0mon.service is een python service die draait in 'systemd'
de service 'luisterd' naar pulsen op een GPIO poort.
Als de service een pulse detecteert knippert er een LED (aangesloten op een 2e GPIO poort).
Het moment van detectie wordt in een tabel opgeslagen ook de equivalente waarde van de energie wordt opgeslagen in een tabel.

Om de SD kaart te ontlasten maak de s0mon service gebruik van de zelfde ramdisk als p1mon.
Tijdelijk worden de gegevens opgeslagen in een database op /p1mon/mnt/ramdisk.
Iedere 15 minuten wordt een backup opgeslagen in /p1mon/data.
Bij afsluiten wordt de laatste tijdelijke versie naar de backup directory geschreven.
Bij opstarten wordt automatisch deze backup teruggezet op de ramdisk.


### Installatie ###
* login op de shell van de Pi
* kopieer *.sh en *.tar.gz naar de home directory van p1mon (/home/p1mon)
* gebruik het command: ./uitpakken_s0mon.sh <zipfile>.tar.gz
* ga naar de directory /s0mon/scripts 
* gebruik het commando: ./install_s0mon_systemd.sh -a  (-a = add    -r = remove)

* De status kan bekeken worden met 
    sudo systemctl status s0mon.service

