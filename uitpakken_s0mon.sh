echo "uitpakken van de zipfile voor s0mon"

if [[ "${1}" == "" ]]; then
    echo "geen zipfile opgegeven!. gebruik:"
    echo " ./uitpakken_s0mon.sh <zipfile>"
    echo "uitpakken gestopt."
    exit 1
fi


tar_file="${1}"
if [[ ! -f "${tar_file}" ]]; then
   echo "kon het bestand ${tar_file}  niet vinden."
    echo "uitpakken gestopt."
    exit 1
fi

echo "  zipfile gevonden ${tar_file}"

cd /
cd /p1mon

tar -xvf "/home/p1mon/${tar_file}"

echo ""
echo "Klaar."


